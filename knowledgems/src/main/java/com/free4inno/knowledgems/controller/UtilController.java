package com.free4inno.knowledgems.controller;

import com.free4inno.knowledgems.dao.AttachmentDao;
import com.free4inno.knowledgems.service.FileService;
import com.free4inno.knowledgems.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.Map;
import com.free4inno.knowledgems.service.ResourceEsService;

/**
 * UtilController.
 */
@Slf4j
@Controller
@RequestMapping("util")
public class UtilController {

    @Autowired
    private AttachmentDao attachmentDao;

    @Autowired
    private FileService fileService;

    /**
     * 上传图片.
     *
     * @param file
     * @param request
     * @return location(图片要上传的位置).
     * @throws .
     * @Title: uploadImage.
     * @Description: 上传图片.
     */
    @RequestMapping("/uploadImage")
    @ResponseBody
    public Map<String, Object> uploadImage(@RequestParam("file") MultipartFile file,
                                           HttpSession session, HttpServletRequest request) throws Exception {
        log.info(this.getClass().getName() + "----in----" + "上传图片(uploadImage)" + "----" + session.getAttribute(Constants.USER_ID));
        // TODO 判断上传是否为图片，富文本编辑器可以以后缀名判断，如后面有需求再加以ImageIO判断的方法

        String path = fileService.uploadFile(file);
        log.info(this.getClass().getName() + "----" + "上传图片路径: " + path + "----" + session.getAttribute(Constants.USER_ID));

        // return
        Map<String, Object> ret = new HashMap<>();
        String location = "/util/downloadImage?id=" + path;
        ret.put("location", location);
        log.info(this.getClass().getName() + "----out----" + "图片上传完毕，返回存储地址" + "----" + session.getAttribute(Constants.USER_ID));
        return ret;
    }

    @ResponseBody
    @RequestMapping(value = "/downloadImage")
    public void downloadImage(@RequestParam("id") String id, @RequestParam(value = "name", required = false, defaultValue = "") String name,
                              HttpSession session, HttpServletResponse resp) {
        log.info(this.getClass().getName() + "----in----" + "下载图片(downloadImage)" + "----" + session.getAttribute(Constants.USER_ID));
        fileService.downloadFile(name, id, true, resp);
        log.info(this.getClass().getName() + "----out----" + "下载图片完毕" + "----" + session.getAttribute(Constants.USER_ID));
    }

    @ResponseBody
    @RequestMapping(value = "/uploadAttachment", headers = "content-type=multipart/form-data")
    public Map<String, Object> uploadAttachment(@RequestParam("file") MultipartFile[] file,
                                                HttpSession session, HttpServletRequest request) throws Exception {
        log.info(this.getClass().getName() + "----in----" + "传值到后端上传附件函数(uploadAttachment)" + "----" + session.getAttribute(Constants.USER_ID));

        String path = fileService.uploadFile(file[0]);
        log.info(this.getClass().getName() + "----" + "上传附件路径: " + path + "----" + session.getAttribute(Constants.USER_ID));

        // return
        Map<String, Object> ret = new HashMap<>();
        String location = "/util/downloadAttachment?id=" + path;
        ret.put("location", location);
        log.info(this.getClass().getName() + "----out----" + "附件上传完毕，返回存储地址" + "----" + session.getAttribute(Constants.USER_ID));
        return ret;
    }

    @ResponseBody
    @RequestMapping(value = "/downloadAttachment")
    public void downloadAttachment(@RequestParam("id") String id, @RequestParam(value = "name", required = false, defaultValue = "") String name,
                                   HttpSession session, HttpServletResponse resp) {
        log.info(this.getClass().getName() + "----in----" + "下载附件(downloadAttachment)" + "----" + session.getAttribute(Constants.USER_ID));
        fileService.downloadFile(name, id, false, resp);
        log.info(this.getClass().getName() + "----out----" + "下载附件完毕" + "----" + session.getAttribute(Constants.USER_ID));
    }

    @ResponseBody
    @RequestMapping("/deleteAttachment")
    public Map<String, Object> deleteAttachment(@RequestParam("saveUrl") String saveUrl,
                                                HttpSession session, HttpServletRequest request) throws Exception {
        log.info(this.getClass().getName() + "----in----" + "删除附件(deleteAttachment)" + "----" + session.getAttribute(Constants.USER_ID));
        //清除数据库中的附件数据
        try {
            attachmentDao.deleteByUrl(saveUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, Object> ret = new HashMap<>();
        if (fileService.deleteFile(saveUrl)) {
            ret.put("result", "SUCCESS");
            log.info(this.getClass().getName() + "----out----" + "删除附件成功" + "----" + session.getAttribute(Constants.USER_ID));
        } else {
            ret.put("result", "ERROR");
            log.info(this.getClass().getName() + "----in----" + "删除附件错误" + "----" + session.getAttribute(Constants.USER_ID));
        }
        return ret;
    }


}
