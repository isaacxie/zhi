package com.free4inno.knowledgems.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;


/**
 * Author HaoYi.
 * Date 2020/9/11.
 */

@Entity
@Table(name = "label_info")
@Data
public class LabelInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "label_id")
    private Integer id; //主键

    @Column(name = "crop_id")
    private Integer cropId; //CropId

    @Column(name = "label_name")
    private String labelName; //标签名

    @Column(name = "label_info", columnDefinition = "TEXT")
    private String labelInfo; //用户组描述

    @Column(name = "create_time")
    private Timestamp createTime; //资源创建时间

    @Column(name = "edit_time")
    private Timestamp editTime; //资源修改时间

    @Column(name = "uplevel_id")
    private Integer uplevelId; //默认为0，即为一级标签，如果不为零则为对应label_id的下级标签

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCropId() {
        return cropId;
    }

    public void setCropId(Integer cropId) {
        this.cropId = cropId;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getLabelInfo() {
        return labelInfo;
    }

    public void setLabelInfo(String labelInfo) {
        this.labelInfo = labelInfo;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getEditTime() {
        return editTime;
    }

    public void setEditTime(Timestamp editTime) {
        this.editTime = editTime;
    }

    public Integer getUplevelId() {
        return uplevelId;
    }

    public void setUplevelId(Integer uplevelId) {
        this.uplevelId = uplevelId;
    }

    public LabelInfo() {

    }

    public LabelInfo(int id) {
        this.id = id;
    }
}
