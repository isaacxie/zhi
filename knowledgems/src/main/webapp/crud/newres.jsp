<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/9/4
  Time: 15:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>新建资源-知了[团队知识管理应用]</title>

    <%--tinymce--%>
    <script src="../assets/vendor/tinymce/tinymce.min.js"></script>
    <script src="../assets/vendor/tinymce/langs/zh_CN.js"></script>
</head>

<body>
<div class="re-main">
    <c:import url="../template/_navbar.jsp?menu=newres"/>
    <div class="re-box-6 re-grey mt-85">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-xl-12">
                    <div class="re-box-decorated">
                        <div class="re-box-content">
                            <%---------------------- 标题 ----------------------%>
                            <div class="re-form-group">
                                <span class="mnt-7 font-2"><strong>&nbsp;标&nbsp;题&nbsp;</strong></span>
                                <input type="text" value="${resource.title}" class="form-control form-control-style-2"
                                       id="title" placeholder="请输入资源标题，内容不可为空">
                            </div>
                            <br/>
                            <%---------------------- 群组 ----------------------%>
                            <div class="re-form-group">
                                <span class="mnt-7 font-2"><strong>&nbsp;群&nbsp;组&nbsp;</strong></span>
                                <div class="re-form-group">
                                    <c:forEach var="groupList" items="${groupInfoList}">
                                        <button id="group${groupList.id}" class="re-btn re-btn-unselected"
                                                onclick="groupButton(${groupList.id})">${groupList.groupName}</button>
                                    </c:forEach>
                                </div>
                            </div>
                            <br/>
                            <%---------------------- 模板 ----------------------%>
                            <div class="re-form-group" style="line-height:31px;">
                                <span class="mnt-7 font-2"><strong>&nbsp;模&nbsp;板&nbsp;</strong></span>
                                <div class="re-form-group">
                                    <c:forEach var="templateList" items="${templateInfoList}">
                                        <button id="template${templateList.id}" class="re-btn re-btn-unselected"
                                                onclick="templateButton(${templateList.id})">${templateList.templateName}</button>
                                    </c:forEach>
                                </div>
                            </div>
                            <br/>
                            <%---------------------- 正文编辑 ----------------------%>
                            <div>
                                <div id="editor" autofocus></div>
                            </div>
                            <%---------------------- 书籍目录 ----------------------%>
                            <div class="re-form-group">
                                <textarea class="form-control re-hide book-contents-input" id="contents"
                                          rows="3" placeholder="请按规定格式编辑书籍目录"></textarea>
                            </div>
                            <br/>
                            <%---------------------- 标签 ----------------------%>
                            <div class="re-form-group">
                                <span class="mnt-7 font-2"><strong>&nbsp;标&nbsp;签&nbsp;</strong></span>
                                <div class="re-form-group" id="labels">
                                </div>
                            </div>
                            <br/>
                            <%---------------------- 附件 ----------------------%>
                            <div class="re-form-group">
                                <div style="line-height:62px;position:relative;">
                                    <span class="mnt-7 font-2"><strong>&nbsp;附&nbsp;件&nbsp;</strong></span>
                                </div>
                                <input style="display: none" type="file" class="form-control form-control-style-2"
                                       id="attachment" onchange="fileBtnResult()">
                                <div class="font-text-darkgray attach-box" id="drop-zone" onclick="fileBtn()">
                                    <div>拖拽到框中上传附件或点击选取文件</div>
                                </div>
                                <div id="file-list" style="line-height:38px;margin-top: 15px;">
                                </div>
                                <div id="progress-bar" style="line-height:38px;">
                                </div>
                            </div>
                            <br/>
                            <%---------------------- 保存 ----------------------%>
                            <div class="re-form-group" style="float:right">
                                <button class="re-btn" type="submit" id="save" onclick="submit()"><a class="mnt-7">保&nbsp;存</a></button>
                            </div>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START: Footer -->
    <c:import url="../template/_footer.jsp"/>
    <!-- END: Footer -->
</div>
</body>

<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>

<%-- tinymce --%>
<script src="../assets/vendor/tinymce/tinymce.min.js"></script>
<script language="JavaScript">
    /* 加载动画 */
    loading("show");

    /* 富文本编辑器初始化 */
    tinymce.init({
        selector: '#editor',
        contextmenu: "bold copy | link | image | imagetools | table | spellchecker",//上下文菜单
        auto_focus: true,
        menubar: false,
        language: 'zh_CN',
        // force_br_newlines: false,
        // force_p_newlines: true,
        // forced_root_block: "",
        // preformatted: true,
        toolbar_mode: 'sliding',
        branding: false,
        content_style: "img {height:auto;max-width:100%;max-height:100%;}",
        plugins: [
            'powerpaste', // plugins中，用powerpaste替换原来的paste
            'image',
            "autoresize",
            "code",
            "link",
            "charmap",
            "emoticons",
            "table",
            "wordcount",
            "advlist",
            "lists",
            "indent2em",
            "hr",
            "toolbarsticky",
            "textpattern"
            //...
        ],
        min_height: 400, //编辑区域的最小高度
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt 48pt",
        font_formats: "微软雅黑='微软雅黑';宋体='宋体';黑体='黑体';仿宋='仿宋';楷体='楷体';隶书='隶书';幼圆='幼圆';Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings",
        powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
        powerpaste_html_import: 'propmt',// propmt, merge, clear
        powerpaste_allow_local_images: true,
        paste_data_images: true,
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open("POST", "${pageContext.request.contextPath}/util/uploadImage");
            formData = new FormData();
            formData.append("file", blobInfo.blob(), blobInfo.filename());
            xhr.onload = function (e) {
                var json;
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                json = JSON.parse(this.responseText);

                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                success(json.location);
            };
            xhr.send(formData);
        },
        toolbar: [
            'code insertfile undo redo emoticons | formatselect | forecolor backcolor charmap bold italic underline strikethrough hr | fontselect | fontsizeselect | blockquote subscript superscript removeformat |',
            'bullist numlist | alignleft aligncenter alignright alignjustify outdent indent indent2em | link image table'
        ],
        init_instance_callback: function () {
            loading("reset");
            // var htmlStr = "";
            // $("#loading_editor").html(htmlStr);
        },
        my_toolbar_sticky: true,
        toolbar_sticky_always: true,
        toolbar_sticky_elem: "nav",
        toolbar_sticky_elem_autohide: false
    });

    // *************** 上传附件 BEGIN ***************
    //"附件"字段json封装
    function attachmentJson(attachment, name, url, attaI) {
        j = attachment.length;
        var con = {};
        con["id"] = attaI;
        con["name"] = name;
        con["url"] = url;
        attachment[j] = con;
        return attachment;
    }
    //删除附件
    function deleteAttachment(attachment, i) {
        var saveUrl = attachment[attachment.findIndex(e => e.id === i)].url
        $.post("${pageContext.request.contextPath}/util/deleteAttachment", {
            saveUrl: saveUrl
        });
        attachment.splice(attachment.findIndex(e => e.id === i), 1)
        $("#atta" + i).remove();
        return attachment;
    }
    //”上传附件“按钮
    function fileBtn() {
        document.getElementById('attachment').click();
    }
    var attaName = "";
    //”上传附件“按钮返回值
    function fileBtnResult() {
        //定义表单变量
        var file = document.getElementById('attachment').files;
        if(file.length==0){	//没有文件返回false
            return false
        }
        console.log(file.length);
        //新建一个FormData对象
        var formData = new FormData();
        // //追加文件数据
        // for(i=0;i<file.length;i++){
        //     formData.append("file", file[i]);
        // }
        // console.log(formData);
        formData.append("file", file[0]);
        attaName = file[0].name;
        // 上传
        uploadattachment(formData, attaName)
    }
    //拖拽上传
    var dropZone = document.getElementById('drop-zone');
    //文件拖入框
    dropZone.ondragover = function (e){
        e.preventDefault();  //阻止浏览器默认事件
        e.target.style.border="3px dashed black";
    }
    //文件移除框
    dropZone.ondragleave = function (e){
        e.preventDefault();  //阻止浏览器默认事件
        e.target.style.border="1px dashed darkgray";
    }
    //鼠标松开文件
    dropZone.ondrop = function (e){
        e.preventDefault();  //阻止浏览器默认事件
        e.target.style.border="1px dashed darkgray";
        var file = e.dataTransfer.files
        if(file.length==0){	//没有文件返回false
            return false
        }
        for(i=0; i<file.length; i++){
            //新建一个FormData对象
            var formData = new FormData();
            // //追加文件数据
            // for(i=0;i<file.length;i++){
            //     formData.append("file", file[i]);
            // }
            // console.log(formData);
            formData.append("file", file[i]);
            attaName = file[i].name;
            // 上传
            uploadattachment(formData, attaName)
        }
    }
    //上传附件
    var attachment = new Array();
    var longA;
    var attaI = 0;
    function uploadattachment(formData, attaName) {
        var xhr = new XMLHttpRequest();//第一步
        xhr.open("POST", "${pageContext.request.contextPath}/util/uploadAttachment");
        xhr.onload = function (e) {
            var json;
            var htmlStr = $("#file-list").html();
            var htmlProgress = "";
            if (xhr.status != 200) {
                alert(xhr.status);
                // failure('HTTP Error: ' + xhr.status);
                return;
            }
            json = JSON.parse(this.responseText);

            if (!json || typeof json.location != 'string') {
                alert(xhr.responseText);
                // failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
            attachment = attachmentJson(attachment, attaName, json.location, attaI)
            // longA = attachment.length-1;
            htmlStr += "<div id=\"atta" + attaI + "\" class=\"mnt-7 font-text\"><a>&nbsp;" + attaName + "&nbsp;</a>&nbsp;&nbsp;<a href=\"javascript:void(0)\" onclick=\"deleteAttachment(attachment," + attaI + ")\">[删除]</a></div>";
            attaI = attaI + 1;
            $("#file-list").html(htmlStr);
            $("#progress-bar").html(htmlProgress);
            $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '上传附件成功'});
            // alert("sc"+json.location+file[0].name);
            // success(json.location);
        };
        xhr.upload.onprogress = function (evt) {
            //侦查附件上传情况
            //通过事件对象侦查
            //该匿名函数表达式大概0.1秒执行一次
            var loaded = evt.loaded;//已经上传大小情况
            var tot = evt.total;//附件总大小
            var per = Math.floor(100 * loaded / tot);  //已经上传的百分比
            var htmlProgress = "<a>&nbsp;" + attaName + "&nbsp;&nbsp;&nbsp;</a><progress value=" + per + " max=\"100\"></progress>"
            $("#progress-bar").html(htmlProgress);
            console.log("已上传：" + per + "%")
        };
        xhr.send(formData);
    }
    // *************** 上传附件 END ***************

    /* 书籍目录输入框 高度自适应 */
    let tempScrollTop;
    window.onload = function () {
        /* auto textarea height */
        $('#contents').on('input', function () {
            // bind auto height
            $('#contents').height('auto');
            $('#contents').height((this.scrollHeight) + 'px');
            // set 'document.documentElement.scrollTop' to avoid textarea's shake
            document.documentElement.scrollTop = tempScrollTop;
        }).on('click', function () {
            // get height
            tempScrollTop = document.documentElement.scrollTop;
        }).on('keydown', function () {
            // get height
            tempScrollTop = document.documentElement.scrollTop;
        });
    };

    /* 获取标签 */
    mount();
    function mount() {
        $.ajax({
            type: "get",
            url: "/crudres/getLabel",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.code === 200) {
                    labelList = data.result;
                    load(labelList);
                } else
                    message(data.msg, false);
            }, error: function (err) {
                message("添加失败：" + err, false);
            }
        });
    }

    /* 载入标签 */
    function load(tagList) {
        var htmlStr = "";
        for (var j = 0; j < tagList.length; j++) {
            htmlStr += "<div class=\"col-md-12 row\">";
            for (var i = 0; i < tagList[j].length; i++) {
                switch (i) {
                    case 0: {
                        htmlStr += "<div>";
                        htmlStr += "<a class=\"re-btn re-label-header\" disabled=\"disabled\">" + tagList[j][i].name + "</a>";
                        htmlStr += "</div>";
                        htmlStr += "<div class=\"col-md-10\">";
                        break;
                    }
                    default: {
                        var tagid = tagList[j][i].id;
                        var tempGroup = "label" + tagid;
                        htmlStr += "<div class='float-left'>";
                        htmlStr += "<button id=\"" + tempGroup + "\" class=\"re-btn re-btn-unselected re-label-child\" onclick=\"labelButton(" + tagid + ")\">" + tagList[j][i].name + "</button>";
                        htmlStr += "</div>";
                    }
                }
            }
            htmlStr += "</div>";
            htmlStr += " <br/>";
            htmlStr += "</div>";
        }
        $("#labels").html(htmlStr);
    }

    var groupIds = "";
    var labelIds = "";

    /* 标签按钮切换 */
    function labelButton(id) {
        if ($('#label' + id).hasClass('re-btn-unselected')) {
            $('#label' + id).removeClass('re-btn-unselected');
            labelIds = labelIds + ',' + id;
            console.log("labelIds" + labelIds);
            /* check BOOK */
            if (id == ${bookLabelId}) { $('#contents').removeClass('re-hide'); }
        } else {
            $('#label' + id).addClass('re-btn-unselected');
            labelIds = labelIds.replace(',' + id, '');
            console.log("labelIds" + labelIds);
            /* check BOOK */
            if (id == ${bookLabelId}) { $('#contents').addClass('re-hide'); }
        }
    }

    /* 群组按钮切换 */
    function groupButton(id) {
        if ($('#group' + id).hasClass('re-btn-unselected')) {
            $('#group' + id).removeClass('re-btn-unselected');
            groupIds = groupIds + ',' + id;
            console.log("groupIds" + groupIds);
        } else {
            $('#group' + id).addClass('re-btn-unselected');
            groupIds = groupIds.replace(',' + id, '');
            console.log("groupIds" + groupIds);
        }
    }

    /* 模板功能 */
    var templateId = -1;
    function templateButton(id) {
        if (templateId != id) {
            // use template
            // check
            $.tipModal('confirm', 'warning', '确定应用模板？<br>（注意: 输入框当前内容将被覆盖! ）', function (result) {
                if (result == true) {
                    // do
                    $.ajax({
                        type: "get",
                        url: "../crudres/getTemplateCode?id=" + id,
                        success: function (data) {
                            tinyMCE.activeEditor.setContent(data["templateCode"]);
                            $('#template' + id).removeClass('re-btn-unselected');
                            $('#template' + templateId).addClass('re-btn-unselected');
                            templateId = id;
                            console.log("use templateId " + templateId);
                            $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '应用模板成功！'});
                        },
                        error: function () {
                            $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '应用模板失败！'});
                        }
                    })
                }
            });
        } else {
            // cancel template
            // check
            $.tipModal('confirm', 'warning', '确定取消模板？<br>（注意: 输入框当前内容将被清空! ）', function (result) {
                if (result == true) {
                    // do
                    tinyMCE.activeEditor.setContent("");
                    $('#template' + id).addClass('re-btn-unselected');
                    templateId = -1;
                    console.log("cancel templateId " + templateId);
                }
            });
        }
    }

    /* 提交新建资源 */
    function submit() {
        var title = $("#title").val();
        var text = tinyMCE.activeEditor.getContent();
        var group = groupIds.substring(1);
        var label = labelIds.substring(1);
        var atta = JSON.stringify(attachment);
        var superior = 0;
        var contents = $('#contents').val();

        if (title == "") {
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '资源标题不可为空'});
            return false;
        } else if (text == "") {
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '资源正文不可为空'});
            return false;
        } else {
            loading("show");
            <%--//处理\n换行符--%>
            text = text.replaceAll("\n", "");
            $.post("../crudres/newdata", {
                title: title,
                group: group,
                label: label,
                text: text,
                attachment: atta,
                superior: superior,
                contents: contents
            }, function (data) {
                if (data.msg == 'SUCCESS') {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '新建资源成功'});
                    loading("reset");
                    window.location.href = "<%=request.getContextPath()%>/resource/resource?id=" + data.resourceId;
                } else if (data.msg == '必填项未填写完整') {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '必填项必须填写完成'});
                    loading("reset");
                } else {
                    $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '新建项目失败'});
                    // alert("保存失败0.0");
                    loading("reset");
                }
            });
        }
    }
</script>
<!-- END: Scripts -->
</html>
